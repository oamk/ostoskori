<?php namespace App\Controllers;

class Cart extends BaseController
{
  public function __construct() {
    $session = \Config\Services::session();
    $session->start();
  }

	public function index()
	{

    $data['ostokset'] = $_SESSION['kori'];
    echo view('templates/header',['otsikko' => 'Verkkokauppa']);
    echo view('cart_view',$data);
    echo view('templates/footer');
  }
  
  public function lisaa() {
    $tuote = $this->request->getPost('tuote');
    
    if (!isset($_SESSION['kori'])) {
      $_SESSION['kori'] = array();
    }

    array_push($_SESSION['kori'],$tuote);
    //print_r($_SESSION['kori']);
    return redirect('/');
  }

  public function tyhjenna() {
    $_SESSION['kori'] = null;
    return redirect('/');
  }
  
}