<?php namespace App\Controllers;

class Shop extends BaseController
{

  public function __construct() {
    $session = \Config\Services::session();
    $session->start();
  }

	public function index()
	{
    echo view('templates/header',['otsikko' => 'Verkkokauppa']);
    echo view('shop_view');
    echo view('templates/footer');
  }
  

}