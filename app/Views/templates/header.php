<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title><?= $otsikko?></title>
  </head>
  <body>
    <div class="container">
    <?php
    if (isset($_SESSION['kori'])) {
      echo '<div><a href="' . site_url('cart/index') . '">' . count($_SESSION['kori']) . '</a></div>';
      //echo 'Kori on olemassa';
      //print_r($_SESSION['kori']);
    }
    else {
      echo '<div>0</div>';
    }
